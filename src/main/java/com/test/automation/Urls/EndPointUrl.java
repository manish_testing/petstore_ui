package com.test.automation.Urls;

public enum EndPointUrl {

	GET_WEATHER_BY_CITY_NAME("/weather?q=");
	
	String uri;
	
	EndPointUrl(String uri){
		this.uri=uri;
	}
	
	public String getUri(){
		return this.uri;
	}
	public String getUri(String data){
		return this.uri+data;
	}
}
