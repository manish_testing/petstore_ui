package com.test.automation.WebServiceMethods;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class WebServices {

	public static Response Get(String uRI){
		RequestSpecification req=RestAssured.given();
		req.contentType(ContentType.JSON);
		Response response=req.get(uRI);
		return response;
	}
	
}
