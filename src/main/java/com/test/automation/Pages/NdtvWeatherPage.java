package com.test.automation.Pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.test.automation.Generic.TestBase;

public class NdtvWeatherPage extends TestBase {

	WebDriver driver;
	String city = null;
	public static final Logger log = Logger.getLogger(NdtvWeatherPage.class.getName());
	
	@FindBy(xpath="//a[contains(text(),'No Thanks')]")
	WebElement noThanksPopUp;

	@FindBy(css = "a#h_sub_menu")
	private WebElement subMenu;

	@FindBy(xpath = "//a[contains(text(),'WEATHER')]")
	private WebElement weatherLink;

	@FindBy(css = "input#searchBox")
	private WebElement searchWeather;

	public NdtvWeatherPage(WebDriver driver) {
		//super();
		//super(errorMessage);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void searchCity(String city) {
		try{
		noThanksPopUp=returnWaitforWebElement(driver, noThanksPopUp);
		}catch(Exception e){
			e.printStackTrace();
		}
		//if(noThanksPopUp==null){
		if(noThanksPopUp!=null && noThanksPopUp.isDisplayed()){
			noThanksPopUp.click();
		}
		//}
		subMenu.click();
		WaitforWebElement(driver, weatherLink);
		weatherLink.click();
		WaitforWebElement(driver, searchWeather);
		searchWeather.sendKeys(city);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
