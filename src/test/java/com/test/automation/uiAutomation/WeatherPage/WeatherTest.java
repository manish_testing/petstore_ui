package com.test.automation.uiAutomation.WeatherPage;

import java.io.IOException;
import java.util.Comparator;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.automation.Generic.TestBase;
import com.test.automation.Pages.NdtvWeatherPage;
import com.test.automation.Urls.BaseUrls;
import com.test.automation.Urls.EndPointUrl;
import com.test.automation.WebServiceMethods.WebServices;
import com.test.automation.pojo.Base;
import com.test.automation.pojo.Main;

import io.restassured.response.Response;

public class WeatherTest extends TestBase {

	public WeatherTest(String errorMessage) {
		super(errorMessage);
	}

	
	
	public static final Logger log = Logger.getLogger(WeatherTest.class.getName());
	NdtvWeatherPage weatherPage;
	String cityxpath = null;
	WebElement city1 = null;
	String WindXpath;
	WebElement wind;
	String humidityXpath;
	WebElement humidutyElement;
	Float speedUi;
	int humidityUi;
	Float speedaPI;
	int humidityaPI;
	Base base = null;

	@DataProvider(name = "SignUpData")
	public String[][] getTestData() {
		String[][] testRecords = getData("TestData.xlsx", "CityWeather");
		return testRecords;
	}

	@BeforeClass
	public void setUp() throws IOException {
		init();
	}

	@AfterClass
	public void TearDown() {
		driver.quit();
	}

	@Test(dataProvider = "SignUpData", enabled = true)
	public void getCityTemperatureFromUi(String city) {

		weatherPage = new NdtvWeatherPage(driver);
		city=city.toLowerCase().substring(0, city.length()).replace(city.charAt(0), String.valueOf(city.charAt(0)).toUpperCase().charAt(0));
		System.out.println("City is upperCase is "+city);
		weatherPage.searchCity(city);
		cityxpath = "//label[@for='" + city + "']//input";
		city1 = returnWaitforWebElement(driver, getWebElement("xpath", cityxpath));
		if (!city1.isSelected()) {
			city1.click();
			cityxpath = "//div[@class='cityText' and contains(text(),'" + city + "')]";
			city1 = returnWaitforWebElement(driver, getWebElement("xpath", cityxpath));
			if (city1.isDisplayed()) {
				city1.click();
				if (driver.findElement(By.xpath("//span[contains(text(),'" + city + "')]")).isDisplayed()) {
					WindXpath = "//b[contains(text(),'Wind')]";
					wind = returnWaitforWebElement(driver, getWebElement("xpath", WindXpath));
					if (wind.isDisplayed()) {
						String speedText = wind.getText();
						speedUi = Float.valueOf(speedText.split(":")[1].trim().split(" ")[0].trim());
						humidityXpath = "//b[contains(text(),'Wind')]//following::b[1]";
						humidutyElement = returnWaitforWebElement(driver, getWebElement("xpath", humidityXpath));
						if (humidutyElement.isDisplayed()) {
							humidityUi = Integer
									.parseInt(humidutyElement.getText().split(":")[1].replace("%", "").trim());
						}
						System.out
								.println("Speed as per UI ==> " + speedUi + " and humidity is ==> " + humidityUi + "");
					}
				}

			}
		}
		String Uri = BaseUrls.fixUrl
				+ EndPointUrl.GET_WEATHER_BY_CITY_NAME.getUri("" + city + "&appid=7fe67bf08c80ded756e598d6f8fedaea");
		System.out.println("endpoint url is ==> " + Uri);
		Response response = WebServices.Get(Uri);
		if (response.getStatusCode() == 200) {
			Gson gson = new GsonBuilder().create();
			base = gson.fromJson(response.getBody().asString(), Base.class);
			speedaPI = base.getWind().getSpeed();
			humidityaPI = base.getMain().getHumidity();
			System.out.println("speed = " + speedaPI + " humidity => " + humidityaPI + "");

		}

		if ((speedUi - speedaPI) > 2) {
			throw new TestBase("Difference of wind speed is greater than 2 ");
		}
		if ((humidityUi - humidityaPI) > 2 || (humidityaPI-humidityUi)>2) {
			throw new TestBase("Difference of humidity is greater than 2 ");
		}

	}

}
